const header = $('#header');

$(window).on('load', () => {
	var swiper = new Swiper(".characters .swiper-container", {
        slidesPerView:1.3,
        centeredSlides:true,
        loop:true,
        spaceBetween: 18,
        navigation:{
            prevEl: '.characters .swiper-container .swiper-button-prev',
            nextEl: '.characters .swiper-container .swiper-button-next',
        },
        breakpointsInverted: true,
        breakpoints:{
            475:{
                slidesPerView:1.6,
                spaceBetween:35
            },
            768:{
                slidesPerView: 2.2,
                centeredSlides:false,
                loop:false
            },
            1000:{
                slidesPerView:3,
                centeredSlides:false,
                loop:false
            }
        }
    })

    $(".goTop").on('click',function(){
        $('html, body').animate({
            scrollTop: 0
        }, 2000)
    })

    $(".form form input, .form form textarea").on('focus', function(){
        $(this).addClass('valid')
        $(this).parent().removeClass("error")
    })

    $(".form form input, .form form textarea").on('blur', function(){
        if($(this).val() == ""){
            $(this).removeClass('valid')
        }

        if($(this)[0].type == "email" && $(this).val() != "" && IsEmail($(this).val()) === false ){
            $(this).parent().addClass("error")

            iziToast.error({
                title:"Ops...",
                message: "Formato de e-mail inválido, preencha corretamente"
            })
        }

    })
   

    $(".form form").on('submit', function(){
        var fields = $(this).serializeArray()
        var errors = 0
        
        fields.forEach((item,index) =>{
            if(item.value == ""){
                $(".form form label:eq("+index+")").addClass("error")
                
                iziToast.error({
                    title:"Ops...",
                    message: "Campo '"+item.name.toUpperCase()+"' obrigatório, preencha corretamente"
                })

                errors++
            }

            if(item.name == "email" && IsEmail(item.value) === false){
                iziToast.error({
                    title:"Ops...",
                    message: "Formato de e-mail inválido, preencha corretamente"
                })

                errors++
            }
        })

        if(errors == 0){
            iziToast.success({
                title: "Obrigado!",
                message: "Obrigado por entrar em contato. Em breve retornaremos"
            })

            $(this)[0].reset()
        }

        

        return false

        
    })

    function IsEmail(email){
        return /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test(email);
    }
});
