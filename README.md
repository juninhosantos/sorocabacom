# SUPERGIANTGAMES

Para desenvolver este teste foi utilizado o static generator GoPablo.
Siga os passos abaixo para rodar o projeto em dev.

### Instalar GoPablo
```
sudo npm i gopablo -g
```

### Instalar dependências
```
npm install
```

### Rodar em dev
```
npm run dev
```

### Buildar para produção
```
npm run prod
```
Pronto para deploy

---